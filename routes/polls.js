const express = require('express');
const csrf = require('csurf');
const mongoose = require('mongoose');

const Poll = require('../models/poll');

const router = express.Router();
const csrfProtection = csrf();

router.use(csrfProtection);

// Add new option to the Poll if user is authenticated
router.post('/add-option', (req, res, next) => {
  const pollId = req.body.pollId;
  req.checkBody('option', 'You should enter an option.').notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    let messages = [];
    errors.forEach((error) => {
      messages.push(error.msg);
    });
    req.flash('error', messages);
    return res.redirect('/polls/' + pollId);
  }

  if (!req.isAuthenticated()) {
    req.flash('error', ['You should be signed in order to do that.'])
    return res.redirect('/polls/' + pollId)
  }
  
  const newOption = [{name: req.body.option}];

  Poll.update(
    { _id: pollId },
    { $push: { options: { $each: newOption } } },
    (err) => {
      if (err) console.log(err);
      res.redirect('/polls/' + pollId);
    }
  );
});

// Retrieve poll data
router.get('/:id', (req, res, next) => {
  const messages = req.flash('error');
  Poll.aggregate([
    { "$match": { "_id": mongoose.Types.ObjectId(req.params.id) } },
    { "$unwind": { "path": "$options", "preserveNullAndEmptyArrays": true } },
    {
      "$group": {
        "_id": "$_id",
        "totalNumberOfVotes": { "$sum": "$options.votes" },
        "options": { "$push": "$options" },
        "name": { "$first": "$name" },
        "dateCreated": { "$first": "$dateCreated" },
        "author": { "$first": "$author" }
      } 
    }
  ]).exec((err, data) => {
    if (err) console.log(err);
    const result = data[0];
    
    // get a value of 1 vote on bar
    const divValue = 100 / result.totalNumberOfVotes;
    
    // get data based on divValue for bar chart
    const progressBarsData = result.options.map((el) => {
      const pair = {};
      pair.width = el.votes * divValue;
      pair.votes = el.votes;
      pair.name = el.name;
      return pair;
    });

    res.render('polls/poll', {
      pollName: result.name,
      pollOptions: result.options,
      showProgress: result.totalNumberOfVotes > 0,
      progressBarsData: progressBarsData,
      pollId: result._id,
      csrfToken: req.csrfToken(),
      hasErrors: messages.length > 0,
      messages: messages
    });
  });
});

// Vote for a poll
router.post('/:id', (req, res, next) => {
  const submitedOptionId = req.body.option;
  const pollId = req.params.id;

  if (typeof req.session.votes === 'undefined') {
    req.session.votes = {};
  } else if (req.session.votes[pollId]) {
    // provide an array to be able to show two separated errors via view
    req.flash('error', ['You can\'t vote more than once.'])
    return res.redirect('/polls/' + pollId);
  }

  Poll.findById(pollId, (err, poll) => {
    if (err) console.log(err);
    const option = poll.options.id(submitedOptionId);
    option.votes += 1;
    poll.save((err, updatedPoll) => {
      if (err) console.log(err);
      // add {poll: vote} bond to session
      req.session.votes[pollId] = submitedOptionId;
      res.redirect('/polls/' + pollId);
    });
  });
});

module.exports = router;
