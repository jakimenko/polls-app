const express = require('express');

const Poll = require('../models/poll');

const router = express.Router();

router.get('/', (req, res, next) => {
  Poll.find().sort('-dateCreated').exec((err, polls) => {
    res.render('index', {
      polls: polls
    });
  });
});

module.exports = router;
