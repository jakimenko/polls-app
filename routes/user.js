const express = require('express');
const csrf = require('csurf');
const passport = require('passport');

const Poll = require('../models/poll');
const User = require('../models/user');
const authHelpers = require('../helpers-middleware/auth');

const router = express.Router();
const csrfProtection = csrf();

const notSignedIn = authHelpers.notSignedIn;
const isSignedIn = authHelpers.isSignedIn;

router.use(csrfProtection);

// Create a new Poll.
router.get('/new-poll', isSignedIn, (req, res, next) => {
  const messages = req.flash('error');
  res.render('user/new-poll', {
    csrfToken: req.csrfToken(),
    messages: messages,
    hasErrors: messages.length > 0
  });
});

router.post('/new-poll', isSignedIn, (req, res, next) => {
  req.checkBody('poll-name', 'Poll name can\'t be empty').notEmpty();
  req.checkBody('poll-options', 'You should specify at least one option').notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    let messages = [];
    errors.forEach((error) => {
      messages.push(error.msg);
    });
    req.flash('error', messages);
    res.redirect('/user/new-poll');
  } else {
    const pollName = req.body['poll-name'];
    const pollOptions = toPollOptions(req.body['poll-options'].split(','));

    const newPoll = new Poll({
      name: pollName,
      author: req.user.id,
      options: pollOptions
    });
    newPoll.save((err) => {
      if (err) console.log(err);
      res.redirect('/user/new-poll')
    });
  }
});

// User Signup.
router.get('/signup', notSignedIn, (req, res, next) => {
  const messages = req.flash('error');
  res.render('user/signup', {
    csrfToken: req.csrfToken(),
    messages: messages,
    hasErrors: messages.length > 0
  });
});

router.post('/signup', notSignedIn, passport.authenticate('local.signup', {
  successRedirect: '/user',
  failureRedirect: '/user/signup',
  failureFlash: true
}));

// User Signin.
router.get('/signin', notSignedIn, (req, res, next) => {
  const messages = req.flash('error');
  res.render('user/signin', {
    csrfToken: req.csrfToken(), 
    messages: messages, 
    hasErrors: messages.length > 0 
  });
});

router.post('/signin', notSignedIn, passport.authenticate('local.signin', {
  successRedirect: '/user',
  failureRedirect: '/user/signin',
  failureFlash: true
}));

// User profile
router.get('/', isSignedIn, (req, res, next) => {
  const sMessage = req.flash('success');
  const eMessage = req.flash('error');

  User.findOne({_id: req.user.id})
    .populate({
      path: 'polls',
      options: { sort: { dateCreated: -1 } }
    })
    .select('name author dateCreated')
    .exec((err, polls) => {
      if (err) console.log(err);
      res.render('user/profile', {
        eMessage: eMessage,
        sMessage: sMessage,
        csrfToken: req.csrfToken(),
        fName: req.user.fName,
        lName: req.user.lName,
        userEmai: req.user.email,
        polls: polls.polls
      });
    });
});

router.get('/signout', isSignedIn, (req, res, next) => {
  req.logout();
  res.redirect('/');
});

// Delete a poll
router.post('/delete', (req, res, next) => {
  Poll.remove({_id: req.body.pollId}, (err) => {
    if (err) {
      console.log(err);
      req.flash('error', 'Something went wrong. :(')
      return res.redirect('/user');
    }
    req.flash('success', 'Poll was deleted')
    res.redirect('/user')
  });
});

module.exports = router;

//Helpers
const toPollOptions = (arr) => {
  arr = arr.map((el) => {
    return {'name': el};
  });
  return arr;
};
