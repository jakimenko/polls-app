const passport = require('passport');
const strategie = require('passport-local');

const User = require('../models/user');

const LocalStrategy = strategie.Strategy;

passport.serializeUser((user, done) => {
  done(null, user.id)
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});

passport.use('local.signup', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, (req, email, password, done) => {
  req.checkBody('email', 'Invalid Email').notEmpty().isEmail();
  req.checkBody('password', 'Invalid Password').notEmpty().isLength({min: 4});
  const errors = req.validationErrors();
  if (errors) {
    let messages = [];
    errors.forEach((error) => {
      messages.push(error.msg);
    });
    return done(null, false, req.flash('error', messages));
  }
  User.findOne({ email: email }, (err, user) => {
    if (err) {
      return done(err);
    } else if (user) {
      return done(null, false, { message: 'Email is already in use.' });
    }
    const newUser = new User();
    newUser.email = email;
    newUser.password = newUser.encryptPassword(password);
    newUser.fName = req.body.fName;
    newUser.lName = req.body.lName;
    newUser.save((err, user) => {
      if (err) return done(err);
      return done(null, user);
    });
  });
}));

passport.use('local.signin', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, (req, email, password, done) => {
  req.checkBody('email', 'Email can\'t be empty.').notEmpty();
  req.checkBody('password', 'Password can\'t be empty.').notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    let messages = [];
    errors.forEach((error) => {
      messages.push(error.msg);
    });
    return done(null, false, req.flash('error', messages));
  }
  User.findOne({ email: email }, (err, user) => {
    if (err) {
      return done(err);
    } else if (!user) {
      return done(null, false, { message: 'Can\'t find a user.' });
    } else if (!user.validPassword(password)) {
      return done(null, false, { message: 'Password is wrong.' });
    }
    return done(null, user);
  });
}));