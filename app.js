const express = require('express');
const logger = require('morgan');
const path = require('path');
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const session = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const flash = require('connect-flash');
const validator = require('express-validator');
const connectMongo = require('connect-mongo');

const index = require('./routes/index');
const polls = require('./routes/polls');
const user = require('./routes/user');

const MongoStore = connectMongo(session);

const app = express();

mongoose.connect(process.env.MONGO_URI);
require('./config/passport');

app.engine('hbs', exphbs({ defaultLayout: 'layout', extname: '.hbs' }));
app.set('view engine', 'hbs')

app.use(logger('dev'));
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(validator());
app.use(session({
  secret: process.env.APP_SECRET,
  resave: false,
  saveUninitialized: false,
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

//global var to get user status in views
app.use((req, res, next) => {
  res.locals.isAuthenticated = req.isAuthenticated();
  res.locals.session = req.session;
  next();
})

app.use('/polls', polls);
app.use('/user', user);
app.use('/', index);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    err: res.locals.error
  });
});

module.exports = app;
