const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  fName: String,
  lName: String,
  email: { type: String, required: true },
  password: { type: String, required: true }
}, {
  toObject: { virtuals: true },
  toJSON: { virtuals: true }
});

userSchema.virtual('polls', {
  ref: 'Poll',
  localField: '_id',
  foreignField: 'author'
});

userSchema.statics.searchForUser = function (id, cb) {
  return this.findOne({ _id: id }, cb);
}

userSchema.methods.encryptPassword = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(5));
}

userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.model('User', userSchema);
