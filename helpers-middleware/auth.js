exports.isSignedIn = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  req.flash('error', ['You should be signed in order to do that.'])
  res.redirect('/user/signin')
};

exports.notSignedIn = (req, res, next) => {
  if (!req.isAuthenticated()) {
    return next();
  }
  res.redirect('/')
};

 